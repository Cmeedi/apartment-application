﻿namespace Exercise_1_Real
{
    partial class LowIncomeHousingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FirstNameTextBox = new System.Windows.Forms.TextBox();
            this.FirstNameLabel = new System.Windows.Forms.Label();
            this.LastNameTextBox = new System.Windows.Forms.TextBox();
            this.MiddleInitialLabel = new System.Windows.Forms.Label();
            this.MiddleInitialTextBox = new System.Windows.Forms.TextBox();
            this.PersonInfoBox = new System.Windows.Forms.GroupBox();
            this.BirthMonthComboBox = new System.Windows.Forms.ComboBox();
            this.BirthYearComboBox = new System.Windows.Forms.ComboBox();
            this.BirthDateGroupBox = new System.Windows.Forms.GroupBox();
            this.CurrentAddress1TextBox = new System.Windows.Forms.TextBox();
            this.CurrentAddress2TextBox = new System.Windows.Forms.TextBox();
            this.CurrentAddressLabel = new System.Windows.Forms.Label();
            this.HandiCapCheckBox = new System.Windows.Forms.CheckBox();
            this.MonthlyIncomeTextBox = new System.Windows.Forms.TextBox();
            this.MonthlyIncomLabel = new System.Windows.Forms.Label();
            this.OccupantsComboBox = new System.Windows.Forms.ComboBox();
            this.MaleRadioButton = new System.Windows.Forms.RadioButton();
            this.FemaleRadioButton = new System.Windows.Forms.RadioButton();
            this.GenderGroupBox = new System.Windows.Forms.GroupBox();
            this.PhoneNumberTextBox = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.LastNameLabel = new System.Windows.Forms.Label();
            this.SSNLabel = new System.Windows.Forms.Label();
            this.PhoneNumberLabel = new System.Windows.Forms.Label();
            this.AddressGroupBox = new System.Windows.Forms.GroupBox();
            this.OccupantsLabel = new System.Windows.Forms.Label();
            this.OccupancyStartDate = new System.Windows.Forms.DateTimePicker();
            this.OccupancyStartDateLabel = new System.Windows.Forms.Label();
            this.OccupancyEndDateLabel = new System.Windows.Forms.Label();
            this.OccupancyEndDate = new System.Windows.Forms.DateTimePicker();
            this.OccupancyStartRateLabel = new System.Windows.Forms.Label();
            this.OccupancyStartRateTextBox = new System.Windows.Forms.TextBox();
            this.OccupancyEndRateTextBox = new System.Windows.Forms.TextBox();
            this.OccupancyEndRateLabel = new System.Windows.Forms.Label();
            this.LeaseExpectancyGroupBox = new System.Windows.Forms.GroupBox();
            this.ExpectedStartRateLabel = new System.Windows.Forms.Label();
            this.MonthToMonthRadio = new System.Windows.Forms.RadioButton();
            this.ThreeMonthRadio = new System.Windows.Forms.RadioButton();
            this.SixMonthRadio = new System.Windows.Forms.RadioButton();
            this.YearRadio = new System.Windows.Forms.RadioButton();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.RentLabel = new System.Windows.Forms.Label();
            this.PersonInfoBox.SuspendLayout();
            this.BirthDateGroupBox.SuspendLayout();
            this.GenderGroupBox.SuspendLayout();
            this.AddressGroupBox.SuspendLayout();
            this.LeaseExpectancyGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // FirstNameTextBox
            // 
            this.FirstNameTextBox.Location = new System.Drawing.Point(12, 38);
            this.FirstNameTextBox.Name = "FirstNameTextBox";
            this.FirstNameTextBox.Size = new System.Drawing.Size(115, 22);
            this.FirstNameTextBox.TabIndex = 0;
            this.FirstNameTextBox.Text = "Enter First Name";
            // 
            // FirstNameLabel
            // 
            this.FirstNameLabel.AutoSize = true;
            this.FirstNameLabel.Location = new System.Drawing.Point(12, 18);
            this.FirstNameLabel.Name = "FirstNameLabel";
            this.FirstNameLabel.Size = new System.Drawing.Size(76, 17);
            this.FirstNameLabel.TabIndex = 1;
            this.FirstNameLabel.Text = "First Name";
            // 
            // LastNameTextBox
            // 
            this.LastNameTextBox.Location = new System.Drawing.Point(180, 38);
            this.LastNameTextBox.Name = "LastNameTextBox";
            this.LastNameTextBox.Size = new System.Drawing.Size(115, 22);
            this.LastNameTextBox.TabIndex = 2;
            this.LastNameTextBox.Text = "Enter Last Name";
            // 
            // MiddleInitialLabel
            // 
            this.MiddleInitialLabel.AutoSize = true;
            this.MiddleInitialLabel.Location = new System.Drawing.Point(133, 18);
            this.MiddleInitialLabel.Name = "MiddleInitialLabel";
            this.MiddleInitialLabel.Size = new System.Drawing.Size(30, 17);
            this.MiddleInitialLabel.TabIndex = 5;
            this.MiddleInitialLabel.Text = "M.I.";
            // 
            // MiddleInitialTextBox
            // 
            this.MiddleInitialTextBox.Location = new System.Drawing.Point(133, 38);
            this.MiddleInitialTextBox.Name = "MiddleInitialTextBox";
            this.MiddleInitialTextBox.Size = new System.Drawing.Size(41, 22);
            this.MiddleInitialTextBox.TabIndex = 4;
            this.MiddleInitialTextBox.Text = "M.I.";
            // 
            // PersonInfoBox
            // 
            this.PersonInfoBox.Controls.Add(this.PhoneNumberTextBox);
            this.PersonInfoBox.Controls.Add(this.PhoneNumberLabel);
            this.PersonInfoBox.Controls.Add(this.MonthlyIncomLabel);
            this.PersonInfoBox.Controls.Add(this.textBox1);
            this.PersonInfoBox.Controls.Add(this.MonthlyIncomeTextBox);
            this.PersonInfoBox.Controls.Add(this.SSNLabel);
            this.PersonInfoBox.Controls.Add(this.BirthDateGroupBox);
            this.PersonInfoBox.Controls.Add(this.LastNameTextBox);
            this.PersonInfoBox.Controls.Add(this.HandiCapCheckBox);
            this.PersonInfoBox.Controls.Add(this.FirstNameTextBox);
            this.PersonInfoBox.Controls.Add(this.FirstNameLabel);
            this.PersonInfoBox.Controls.Add(this.LastNameLabel);
            this.PersonInfoBox.Controls.Add(this.GenderGroupBox);
            this.PersonInfoBox.Controls.Add(this.MiddleInitialLabel);
            this.PersonInfoBox.Controls.Add(this.MiddleInitialTextBox);
            this.PersonInfoBox.Location = new System.Drawing.Point(21, 12);
            this.PersonInfoBox.Name = "PersonInfoBox";
            this.PersonInfoBox.Size = new System.Drawing.Size(462, 188);
            this.PersonInfoBox.TabIndex = 9;
            this.PersonInfoBox.TabStop = false;
            this.PersonInfoBox.Text = "Person Information";
            // 
            // BirthMonthComboBox
            // 
            this.BirthMonthComboBox.FormattingEnabled = true;
            this.BirthMonthComboBox.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.BirthMonthComboBox.Location = new System.Drawing.Point(8, 21);
            this.BirthMonthComboBox.Name = "BirthMonthComboBox";
            this.BirthMonthComboBox.Size = new System.Drawing.Size(121, 24);
            this.BirthMonthComboBox.TabIndex = 11;
            this.BirthMonthComboBox.Text = "Month";
            // 
            // BirthYearComboBox
            // 
            this.BirthYearComboBox.FormattingEnabled = true;
            this.BirthYearComboBox.Items.AddRange(new object[] {
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997",
            "1998",
            "1999",
            "2000",
            "2001",
            "2002",
            "2003"});
            this.BirthYearComboBox.Location = new System.Drawing.Point(145, 21);
            this.BirthYearComboBox.Name = "BirthYearComboBox";
            this.BirthYearComboBox.Size = new System.Drawing.Size(121, 24);
            this.BirthYearComboBox.TabIndex = 12;
            this.BirthYearComboBox.Text = "Year";
            // 
            // BirthDateGroupBox
            // 
            this.BirthDateGroupBox.Controls.Add(this.BirthYearComboBox);
            this.BirthDateGroupBox.Controls.Add(this.BirthMonthComboBox);
            this.BirthDateGroupBox.Location = new System.Drawing.Point(5, 78);
            this.BirthDateGroupBox.Name = "BirthDateGroupBox";
            this.BirthDateGroupBox.Size = new System.Drawing.Size(281, 53);
            this.BirthDateGroupBox.TabIndex = 13;
            this.BirthDateGroupBox.TabStop = false;
            this.BirthDateGroupBox.Text = "Birth Date";
            // 
            // CurrentAddress1TextBox
            // 
            this.CurrentAddress1TextBox.Location = new System.Drawing.Point(6, 38);
            this.CurrentAddress1TextBox.Name = "CurrentAddress1TextBox";
            this.CurrentAddress1TextBox.Size = new System.Drawing.Size(231, 22);
            this.CurrentAddress1TextBox.TabIndex = 6;
            this.CurrentAddress1TextBox.Text = "Current Address 1";
            this.CurrentAddress1TextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // CurrentAddress2TextBox
            // 
            this.CurrentAddress2TextBox.Location = new System.Drawing.Point(6, 66);
            this.CurrentAddress2TextBox.Name = "CurrentAddress2TextBox";
            this.CurrentAddress2TextBox.Size = new System.Drawing.Size(231, 22);
            this.CurrentAddress2TextBox.TabIndex = 7;
            this.CurrentAddress2TextBox.Text = "Current Address 2";
            // 
            // CurrentAddressLabel
            // 
            this.CurrentAddressLabel.AutoSize = true;
            this.CurrentAddressLabel.Location = new System.Drawing.Point(6, 18);
            this.CurrentAddressLabel.Name = "CurrentAddressLabel";
            this.CurrentAddressLabel.Size = new System.Drawing.Size(111, 17);
            this.CurrentAddressLabel.TabIndex = 8;
            this.CurrentAddressLabel.Text = "Current Address";
            // 
            // HandiCapCheckBox
            // 
            this.HandiCapCheckBox.AutoSize = true;
            this.HandiCapCheckBox.Location = new System.Drawing.Point(311, 99);
            this.HandiCapCheckBox.Name = "HandiCapCheckBox";
            this.HandiCapCheckBox.Size = new System.Drawing.Size(90, 21);
            this.HandiCapCheckBox.TabIndex = 10;
            this.HandiCapCheckBox.Text = "Handicap";
            this.HandiCapCheckBox.UseVisualStyleBackColor = true;
            this.HandiCapCheckBox.CheckedChanged += new System.EventHandler(this.HandiCapCheckBox_CheckedChanged);
            // 
            // MonthlyIncomeTextBox
            // 
            this.MonthlyIncomeTextBox.Location = new System.Drawing.Point(308, 154);
            this.MonthlyIncomeTextBox.Name = "MonthlyIncomeTextBox";
            this.MonthlyIncomeTextBox.Size = new System.Drawing.Size(102, 22);
            this.MonthlyIncomeTextBox.TabIndex = 14;
            this.MonthlyIncomeTextBox.Text = "Monthly Income";
            this.MonthlyIncomeTextBox.TextChanged += new System.EventHandler(this.MonthlyIncomeTextBox_TextChanged);
            // 
            // MonthlyIncomLabel
            // 
            this.MonthlyIncomLabel.AutoSize = true;
            this.MonthlyIncomLabel.Location = new System.Drawing.Point(308, 134);
            this.MonthlyIncomLabel.Name = "MonthlyIncomLabel";
            this.MonthlyIncomLabel.Size = new System.Drawing.Size(103, 17);
            this.MonthlyIncomLabel.TabIndex = 9;
            this.MonthlyIncomLabel.Text = "Monthy Income";
            // 
            // OccupantsComboBox
            // 
            this.OccupantsComboBox.FormattingEnabled = true;
            this.OccupantsComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.OccupantsComboBox.Location = new System.Drawing.Point(282, 64);
            this.OccupantsComboBox.Name = "OccupantsComboBox";
            this.OccupantsComboBox.Size = new System.Drawing.Size(121, 24);
            this.OccupantsComboBox.TabIndex = 15;
            this.OccupantsComboBox.Text = "Occupants";
            // 
            // MaleRadioButton
            // 
            this.MaleRadioButton.AutoSize = true;
            this.MaleRadioButton.Location = new System.Drawing.Point(6, 21);
            this.MaleRadioButton.Name = "MaleRadioButton";
            this.MaleRadioButton.Size = new System.Drawing.Size(59, 21);
            this.MaleRadioButton.TabIndex = 6;
            this.MaleRadioButton.TabStop = true;
            this.MaleRadioButton.Text = "Male";
            this.MaleRadioButton.UseVisualStyleBackColor = true;
            // 
            // FemaleRadioButton
            // 
            this.FemaleRadioButton.AutoSize = true;
            this.FemaleRadioButton.Location = new System.Drawing.Point(71, 21);
            this.FemaleRadioButton.Name = "FemaleRadioButton";
            this.FemaleRadioButton.Size = new System.Drawing.Size(75, 21);
            this.FemaleRadioButton.TabIndex = 7;
            this.FemaleRadioButton.TabStop = true;
            this.FemaleRadioButton.Text = "Female";
            this.FemaleRadioButton.UseVisualStyleBackColor = true;
            this.FemaleRadioButton.CheckedChanged += new System.EventHandler(this.FemaleRadioButton_CheckedChanged);
            // 
            // GenderGroupBox
            // 
            this.GenderGroupBox.Controls.Add(this.FemaleRadioButton);
            this.GenderGroupBox.Controls.Add(this.MaleRadioButton);
            this.GenderGroupBox.Location = new System.Drawing.Point(305, 21);
            this.GenderGroupBox.Name = "GenderGroupBox";
            this.GenderGroupBox.Size = new System.Drawing.Size(150, 48);
            this.GenderGroupBox.TabIndex = 6;
            this.GenderGroupBox.TabStop = false;
            this.GenderGroupBox.Text = "Gender";
            this.GenderGroupBox.Enter += new System.EventHandler(this.GenderGroupBox_Enter);
            // 
            // PhoneNumberTextBox
            // 
            this.PhoneNumberTextBox.Location = new System.Drawing.Point(136, 154);
            this.PhoneNumberTextBox.Name = "PhoneNumberTextBox";
            this.PhoneNumberTextBox.Size = new System.Drawing.Size(95, 22);
            this.PhoneNumberTextBox.TabIndex = 16;
            this.PhoneNumberTextBox.Text = "(###)###-####";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 154);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(85, 22);
            this.textBox1.TabIndex = 17;
            this.textBox1.Text = "###-##-####";
            // 
            // LastNameLabel
            // 
            this.LastNameLabel.AutoSize = true;
            this.LastNameLabel.Location = new System.Drawing.Point(177, 18);
            this.LastNameLabel.Name = "LastNameLabel";
            this.LastNameLabel.Size = new System.Drawing.Size(76, 17);
            this.LastNameLabel.TabIndex = 3;
            this.LastNameLabel.Text = "Last Name";
            this.LastNameLabel.Click += new System.EventHandler(this.LastNameLabel_Click);
            // 
            // SSNLabel
            // 
            this.SSNLabel.AutoSize = true;
            this.SSNLabel.Location = new System.Drawing.Point(12, 134);
            this.SSNLabel.Name = "SSNLabel";
            this.SSNLabel.Size = new System.Drawing.Size(36, 17);
            this.SSNLabel.TabIndex = 14;
            this.SSNLabel.Text = "SSN";
            // 
            // PhoneNumberLabel
            // 
            this.PhoneNumberLabel.AutoSize = true;
            this.PhoneNumberLabel.Location = new System.Drawing.Point(133, 134);
            this.PhoneNumberLabel.Name = "PhoneNumberLabel";
            this.PhoneNumberLabel.Size = new System.Drawing.Size(103, 17);
            this.PhoneNumberLabel.TabIndex = 17;
            this.PhoneNumberLabel.Text = "Phone Number";
            // 
            // AddressGroupBox
            // 
            this.AddressGroupBox.Controls.Add(this.OccupancyEndRateTextBox);
            this.AddressGroupBox.Controls.Add(this.OccupancyEndRateLabel);
            this.AddressGroupBox.Controls.Add(this.OccupancyStartRateTextBox);
            this.AddressGroupBox.Controls.Add(this.OccupancyStartRateLabel);
            this.AddressGroupBox.Controls.Add(this.OccupancyEndDateLabel);
            this.AddressGroupBox.Controls.Add(this.OccupancyEndDate);
            this.AddressGroupBox.Controls.Add(this.OccupancyStartDateLabel);
            this.AddressGroupBox.Controls.Add(this.OccupantsLabel);
            this.AddressGroupBox.Controls.Add(this.OccupancyStartDate);
            this.AddressGroupBox.Controls.Add(this.CurrentAddressLabel);
            this.AddressGroupBox.Controls.Add(this.OccupantsComboBox);
            this.AddressGroupBox.Controls.Add(this.CurrentAddress1TextBox);
            this.AddressGroupBox.Controls.Add(this.CurrentAddress2TextBox);
            this.AddressGroupBox.Location = new System.Drawing.Point(21, 216);
            this.AddressGroupBox.Name = "AddressGroupBox";
            this.AddressGroupBox.Size = new System.Drawing.Size(462, 198);
            this.AddressGroupBox.TabIndex = 16;
            this.AddressGroupBox.TabStop = false;
            this.AddressGroupBox.Text = "Address";
            // 
            // OccupantsLabel
            // 
            this.OccupantsLabel.AutoSize = true;
            this.OccupantsLabel.Location = new System.Drawing.Point(279, 44);
            this.OccupantsLabel.Name = "OccupantsLabel";
            this.OccupantsLabel.Size = new System.Drawing.Size(76, 17);
            this.OccupantsLabel.TabIndex = 17;
            this.OccupantsLabel.Text = "Occupants";
            // 
            // OccupancyStartDate
            // 
            this.OccupancyStartDate.Location = new System.Drawing.Point(6, 113);
            this.OccupancyStartDate.Name = "OccupancyStartDate";
            this.OccupancyStartDate.Size = new System.Drawing.Size(200, 22);
            this.OccupancyStartDate.TabIndex = 17;
            // 
            // OccupancyStartDateLabel
            // 
            this.OccupancyStartDateLabel.AutoSize = true;
            this.OccupancyStartDateLabel.Location = new System.Drawing.Point(12, 93);
            this.OccupancyStartDateLabel.Name = "OccupancyStartDateLabel";
            this.OccupancyStartDateLabel.Size = new System.Drawing.Size(147, 17);
            this.OccupancyStartDateLabel.TabIndex = 18;
            this.OccupancyStartDateLabel.Text = "Occupancy Start Date";
            // 
            // OccupancyEndDateLabel
            // 
            this.OccupancyEndDateLabel.AutoSize = true;
            this.OccupancyEndDateLabel.Location = new System.Drawing.Point(11, 141);
            this.OccupancyEndDateLabel.Name = "OccupancyEndDateLabel";
            this.OccupancyEndDateLabel.Size = new System.Drawing.Size(147, 17);
            this.OccupancyEndDateLabel.TabIndex = 20;
            this.OccupancyEndDateLabel.Text = "Occupancy Start Date";
            // 
            // OccupancyEndDate
            // 
            this.OccupancyEndDate.Location = new System.Drawing.Point(5, 161);
            this.OccupancyEndDate.Name = "OccupancyEndDate";
            this.OccupancyEndDate.Size = new System.Drawing.Size(200, 22);
            this.OccupancyEndDate.TabIndex = 19;
            // 
            // OccupancyStartRateLabel
            // 
            this.OccupancyStartRateLabel.AutoSize = true;
            this.OccupancyStartRateLabel.Location = new System.Drawing.Point(279, 93);
            this.OccupancyStartRateLabel.Name = "OccupancyStartRateLabel";
            this.OccupancyStartRateLabel.Size = new System.Drawing.Size(147, 17);
            this.OccupancyStartRateLabel.TabIndex = 21;
            this.OccupancyStartRateLabel.Text = "Occupancy Start Rate";
            // 
            // OccupancyStartRateTextBox
            // 
            this.OccupancyStartRateTextBox.Location = new System.Drawing.Point(282, 113);
            this.OccupancyStartRateTextBox.Name = "OccupancyStartRateTextBox";
            this.OccupancyStartRateTextBox.Size = new System.Drawing.Size(102, 22);
            this.OccupancyStartRateTextBox.TabIndex = 18;
            this.OccupancyStartRateTextBox.Text = "Start Rate";
            // 
            // OccupancyEndRateTextBox
            // 
            this.OccupancyEndRateTextBox.Location = new System.Drawing.Point(282, 158);
            this.OccupancyEndRateTextBox.Name = "OccupancyEndRateTextBox";
            this.OccupancyEndRateTextBox.Size = new System.Drawing.Size(102, 22);
            this.OccupancyEndRateTextBox.TabIndex = 22;
            this.OccupancyEndRateTextBox.Text = "End Rate";
            // 
            // OccupancyEndRateLabel
            // 
            this.OccupancyEndRateLabel.AutoSize = true;
            this.OccupancyEndRateLabel.Location = new System.Drawing.Point(279, 138);
            this.OccupancyEndRateLabel.Name = "OccupancyEndRateLabel";
            this.OccupancyEndRateLabel.Size = new System.Drawing.Size(142, 17);
            this.OccupancyEndRateLabel.TabIndex = 23;
            this.OccupancyEndRateLabel.Text = "Occupancy End Rate";
            // 
            // LeaseExpectancyGroupBox
            // 
            this.LeaseExpectancyGroupBox.Controls.Add(this.RentLabel);
            this.LeaseExpectancyGroupBox.Controls.Add(this.YearRadio);
            this.LeaseExpectancyGroupBox.Controls.Add(this.SixMonthRadio);
            this.LeaseExpectancyGroupBox.Controls.Add(this.ThreeMonthRadio);
            this.LeaseExpectancyGroupBox.Controls.Add(this.MonthToMonthRadio);
            this.LeaseExpectancyGroupBox.Controls.Add(this.ExpectedStartRateLabel);
            this.LeaseExpectancyGroupBox.Location = new System.Drawing.Point(21, 431);
            this.LeaseExpectancyGroupBox.Name = "LeaseExpectancyGroupBox";
            this.LeaseExpectancyGroupBox.Size = new System.Drawing.Size(462, 89);
            this.LeaseExpectancyGroupBox.TabIndex = 17;
            this.LeaseExpectancyGroupBox.TabStop = false;
            this.LeaseExpectancyGroupBox.Text = "Lease Expectancy";
            // 
            // ExpectedStartRateLabel
            // 
            this.ExpectedStartRateLabel.AutoSize = true;
            this.ExpectedStartRateLabel.Location = new System.Drawing.Point(6, 30);
            this.ExpectedStartRateLabel.Name = "ExpectedStartRateLabel";
            this.ExpectedStartRateLabel.Size = new System.Drawing.Size(134, 17);
            this.ExpectedStartRateLabel.TabIndex = 25;
            this.ExpectedStartRateLabel.Text = "Expected Start Rate";
            this.ExpectedStartRateLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // MonthToMonthRadio
            // 
            this.MonthToMonthRadio.AutoSize = true;
            this.MonthToMonthRadio.Location = new System.Drawing.Point(194, 30);
            this.MonthToMonthRadio.Name = "MonthToMonthRadio";
            this.MonthToMonthRadio.Size = new System.Drawing.Size(127, 21);
            this.MonthToMonthRadio.TabIndex = 26;
            this.MonthToMonthRadio.TabStop = true;
            this.MonthToMonthRadio.Text = "Month to Month";
            this.MonthToMonthRadio.UseVisualStyleBackColor = true;
            // 
            // ThreeMonthRadio
            // 
            this.ThreeMonthRadio.AutoSize = true;
            this.ThreeMonthRadio.Location = new System.Drawing.Point(327, 30);
            this.ThreeMonthRadio.Name = "ThreeMonthRadio";
            this.ThreeMonthRadio.Size = new System.Drawing.Size(80, 21);
            this.ThreeMonthRadio.TabIndex = 27;
            this.ThreeMonthRadio.TabStop = true;
            this.ThreeMonthRadio.Text = "3 Month";
            this.ThreeMonthRadio.UseVisualStyleBackColor = true;
            this.ThreeMonthRadio.CheckedChanged += new System.EventHandler(this.ThreeMonthLabel_CheckedChanged);
            // 
            // SixMonthRadio
            // 
            this.SixMonthRadio.AutoSize = true;
            this.SixMonthRadio.Location = new System.Drawing.Point(194, 52);
            this.SixMonthRadio.Name = "SixMonthRadio";
            this.SixMonthRadio.Size = new System.Drawing.Size(80, 21);
            this.SixMonthRadio.TabIndex = 28;
            this.SixMonthRadio.TabStop = true;
            this.SixMonthRadio.Text = "6 Month";
            this.SixMonthRadio.UseVisualStyleBackColor = true;
            // 
            // YearRadio
            // 
            this.YearRadio.AutoSize = true;
            this.YearRadio.Location = new System.Drawing.Point(327, 52);
            this.YearRadio.Name = "YearRadio";
            this.YearRadio.Size = new System.Drawing.Size(59, 21);
            this.YearRadio.TabIndex = 29;
            this.YearRadio.TabStop = true;
            this.YearRadio.Text = "Year";
            this.YearRadio.UseVisualStyleBackColor = true;
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(308, 526);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(83, 31);
            this.OKButton.TabIndex = 19;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(397, 526);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(93, 31);
            this.CancelButton.TabIndex = 20;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // RentLabel
            // 
            this.RentLabel.BackColor = System.Drawing.SystemColors.Window;
            this.RentLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RentLabel.Location = new System.Drawing.Point(6, 52);
            this.RentLabel.Name = "RentLabel";
            this.RentLabel.Size = new System.Drawing.Size(128, 21);
            this.RentLabel.TabIndex = 24;
            this.RentLabel.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // LowIncomeHousingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 569);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.LeaseExpectancyGroupBox);
            this.Controls.Add(this.AddressGroupBox);
            this.Controls.Add(this.PersonInfoBox);
            this.Name = "LowIncomeHousingForm";
            this.Text = "Low Income Housing Form";
            this.PersonInfoBox.ResumeLayout(false);
            this.PersonInfoBox.PerformLayout();
            this.BirthDateGroupBox.ResumeLayout(false);
            this.GenderGroupBox.ResumeLayout(false);
            this.GenderGroupBox.PerformLayout();
            this.AddressGroupBox.ResumeLayout(false);
            this.AddressGroupBox.PerformLayout();
            this.LeaseExpectancyGroupBox.ResumeLayout(false);
            this.LeaseExpectancyGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox FirstNameTextBox;
        private System.Windows.Forms.Label FirstNameLabel;
        private System.Windows.Forms.TextBox LastNameTextBox;
        private System.Windows.Forms.Label MiddleInitialLabel;
        private System.Windows.Forms.TextBox MiddleInitialTextBox;
        private System.Windows.Forms.GroupBox PersonInfoBox;
        private System.Windows.Forms.ComboBox BirthMonthComboBox;
        private System.Windows.Forms.ComboBox BirthYearComboBox;
        private System.Windows.Forms.GroupBox BirthDateGroupBox;
        private System.Windows.Forms.TextBox CurrentAddress1TextBox;
        private System.Windows.Forms.Label CurrentAddressLabel;
        private System.Windows.Forms.TextBox CurrentAddress2TextBox;
        private System.Windows.Forms.CheckBox HandiCapCheckBox;
        private System.Windows.Forms.TextBox MonthlyIncomeTextBox;
        private System.Windows.Forms.Label MonthlyIncomLabel;
        private System.Windows.Forms.ComboBox OccupantsComboBox;
        private System.Windows.Forms.GroupBox GenderGroupBox;
        private System.Windows.Forms.RadioButton FemaleRadioButton;
        private System.Windows.Forms.RadioButton MaleRadioButton;
        private System.Windows.Forms.TextBox PhoneNumberTextBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label LastNameLabel;
        private System.Windows.Forms.Label SSNLabel;
        private System.Windows.Forms.Label PhoneNumberLabel;
        private System.Windows.Forms.GroupBox AddressGroupBox;
        private System.Windows.Forms.TextBox OccupancyEndRateTextBox;
        private System.Windows.Forms.Label OccupancyEndRateLabel;
        private System.Windows.Forms.TextBox OccupancyStartRateTextBox;
        private System.Windows.Forms.Label OccupancyStartRateLabel;
        private System.Windows.Forms.Label OccupancyEndDateLabel;
        private System.Windows.Forms.DateTimePicker OccupancyEndDate;
        private System.Windows.Forms.Label OccupancyStartDateLabel;
        private System.Windows.Forms.Label OccupantsLabel;
        private System.Windows.Forms.DateTimePicker OccupancyStartDate;
        private System.Windows.Forms.GroupBox LeaseExpectancyGroupBox;
        private System.Windows.Forms.Label ExpectedStartRateLabel;
        private System.Windows.Forms.RadioButton YearRadio;
        private System.Windows.Forms.RadioButton SixMonthRadio;
        private System.Windows.Forms.RadioButton ThreeMonthRadio;
        private System.Windows.Forms.RadioButton MonthToMonthRadio;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Label RentLabel;
    }
}

