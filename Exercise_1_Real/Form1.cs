﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_1_Real
{
    public partial class LowIncomeHousingForm : Form
    {
        public LowIncomeHousingForm()
        {
            InitializeComponent();
        }

        private void LastNameLabel_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void HandiCapCheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void FemaleRadioButton_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void GenderGroupBox_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void ThreeMonthLabel_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                double income;
                double rent;
                
                

                income = int.Parse(MonthlyIncomeTextBox.Text);
                rent = income / 2;
                
                




                DialogResult dr = MessageBox.Show("Are you sure all the information is Correct", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (HandiCapCheckBox.Checked)
                {
                    rent -= 300;
                }

                if (ThreeMonthRadio.Checked)
                {
                    rent -= 400;
                }

                if (SixMonthRadio.Checked)
                {
                    rent -= 500;
                }

                if (YearRadio.Checked)
                {
                    rent -= 600;
                }

                if (dr == DialogResult.Yes)
                {
                    RentLabel.Text = rent.ToString("n2");
                }
            }



            catch
            {
                MessageBox.Show("Please enter a valid income rate of Digits only");
            }
        }

        private void MonthlyIncomeTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
